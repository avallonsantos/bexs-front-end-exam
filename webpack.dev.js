const path = require('path')
const merge = require('webpack-merge')
const commom = require('./webpack.common.js')

// Inicializando configuração do Webpack em desenvolvimento

module.exports = merge(commom, {
    mode: 'development',
    optimization: {
        minimize: false
    },
    devServer: {
        contentBase: './dist',
        compress: false,
        port: 9000
    },
})
