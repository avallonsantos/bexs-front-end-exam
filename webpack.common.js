const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const PostCSSPreset = require('postcss-preset-env')

// Incializando configuração do WebPack Padrão

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'checkout.app.js',
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            // Typescript files
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/typescript',
                        '@babel/preset-env'
                    ],
                    plugins: [
                        '@babel/plugin-proposal-class-properties'
                    ]
                }
            },
            // CSS
            {
                test: /\.(css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'resolve-url-loader',
                    'css-loader'
                ]
            },
            // SCSS
            {
                test: /\.(scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                    'resolve-url-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [require('autoprefixer')],
                            parser: "postcss-scss"
                        }
                    },
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: ['./src/assets/scss/_variables.scss']
                        },
                    },
                ]
            },
            // Image files
            {
                test: /\.(png|jpg?e|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'img/'
                        }
                    }
                ]
            },
            // HTML files
            {
                test: /\.(html)$/,
                use: ['html-loader']
            }
        ]
    },
    plugins:  [
        new MiniCssExtractPlugin({
            filename: 'checkout.app.css',
        }),

        new HtmlWebpackPlugin({
            favicon: './src/assets/public/img/favicon.png',
            template: './src/index.html'
        }),

        new PostCSSPreset({
            stage: 3,
            features: {
                'nesting-rules': true,
            },
            cssnano: {
                normalizeUrl: false
            },
            autoprefixer: {
                grid: true,
            },
        })
    ]
}
