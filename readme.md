# DemoShop Checkout

![Logo Avallon Azevedo](https://avallonazevedo.com.br/assets/logo-avallon.png)

DemoShop Checkout é uma aplicação desenvolvida em VanillaJS para simular uma seção de um site real de pagamentos.

### Ferramentas

  - Para desenvolver essa aplicação, utilizei conceitos de POO, alinhados com:
  - Webpack 4
  - Babel 7
  - TypeScript
  
  Por que essas ferramentas?
  
  - Quis utilizar VanillaJS para desenvolver algo independente de frameworks ou libs complexas. Utilizei de uma série de dependências de desenvolvimento para a criação de um código conciso e suportável em diversos browsers.
  - Optei por usar TypeScript para, claro, adicionar uma tipagem mais forte ao JavaScript visando auxilixar o desenvolvimento orientado a objetos.
  - A versão de desenvolvimento é respaldada pelo Webpack DevServer, enquanto que a de produção é gerada com auxílio do HTML Weback Plugin.
  - Toda configuração do Webpack está dividida em 3 arquivos, reprensentando versões de desenvolvimento, produção e uma configuração mútua, seguindo o styleguide do Webpack.

### Organização

- A aplicação está desenvolvida em módulos, seguindo o padrão do ES6.
- Todos os assets estão dentro da pasta **src**. Os arquivos de estilo e imagens encontram-se dentro da pasta **assets**.
- Dentro da pasta **components**, será possível encontrar os módulos do projeto.
- O módulo principal é o **checkout.component.ts**, responsável pela criação da Classe que gere praticamente toda a aplicação.
- Dentro da pasta **utils** será possível encontrar alguns arquivos de apoio, como validadores customizados para alguns campos do checkout.
- Dentro de **order** será possível encontrar o arquivo que gera um novo pedido e um novo pagamento (em um cenário de uma API pronta e funcional)
- Dentro de **components**, ainda, será possível encontrar o arquivo HTML principal, usado pelo HTML Webpack Plugin para gerar a página de produção, junto com o script JS principal, usado como ponto de partida do Webpack para a criação do script de produção.


### Considerações importantes

- Infelizmente não consegui encontrar a font **SF Text Pro**. A que encontrei, estava inacreditavelmente pesada. Em detrimento disso, optei por utilizar a font **San Francisco Text** que, de acordo com algumas pesquisas, trata-se de uma variação da SF Text Pro. Peço a gentileza de, se possível, desconsiderarem o uso de outra font neste cenário.
- Tentei, ao máximo, segmentar a aplicação por micro commits.
- Tentei criar nomes de classes e funções que realmente fizessem sentido e fossem descritivos para suas respectivas funções.
- Optei por criar classes HTML bastante específicas e seguir sua cadeia de hierarquia no CSS, visando que essa aplicação poderia ser inserida em uma aplicação pronta. Dessa forma, evitaríamos problemas de conflitos.

## Instalação

Sendo uma aplicação VanillaJS, existem poucos requerimentos para instalá-la. Primeiro, basta fazer o clone do repositório ou adicionar o remote. Após, fazer a instalação das dependências. Lembrando que nesse passo em diante é necessário ter instalados o **NodeJS** e o **NPM**.

```
$ npm install
```

Uma vez tendo as dependências de desenvolvimento e produção instaladas, basta usar os respectivos comandos para subir o servidor de desenvimento ou criar os arquivos de produção.

(**Importante**: neste repositório já estão incluídos os arquivos de produção.)

Para subir o servidor e ver o teste em desenvolvimento:

```
$ npm run dev
```

Para gerar os arquivos de produção

```
$ npm run build
```

É isso meus (minhas) queridos (queridas)! Espero que esteja de acordo com o esperado. Por favor, qualquer coisa não hesitem em me contatar. Estarei inteiramente à disposição!
