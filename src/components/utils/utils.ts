/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): utils.ts
 * @package DemoShop/utils
 * --------------------------------------------------------------------------
 */

// Adicionando classes em selects preenchidos

const selects = document.querySelectorAll('.customized-select select')

export const addClassToSelect = () => {
    selects.forEach(select => {
        select.addEventListener('change', () => {
            if((select as HTMLInputElement).value !== '') {
                select.classList.add('filled')
            } else {
                select.classList.remove('filled')
            }
        })
    })
}

addClassToSelect()