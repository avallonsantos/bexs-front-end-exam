/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): date-validator.ts
 * @package DemoShop/utils
 * --------------------------------------------------------------------------
 */

export const dateIsValid = date => {
    let valid = false
    let splitedDate = date.split('/')
    let month = splitedDate[0]
    let year = splitedDate[1]
    let expirationDate = new Date(`${month}/01/${year}`)

    if(expirationDate > new Date()) {
        valid = true
    }

    return valid
}
