/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): mask.ts
 * @package DemoShop/utils
 * --------------------------------------------------------------------------
 */


const maskInputs = {
    maskCard: {
        mask: '0000 0000 0000 0000'
    },
    maskExpirationDate: {
        mask: '00/00'
    },
    maskCVV : {
        mask: '000'
    }
}

export default maskInputs
