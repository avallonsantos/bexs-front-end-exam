/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): checkout.component.ts
 * @package DemoShop/services
 * --------------------------------------------------------------------------
 */


// Importando url da api

import DS_API_URL from './url.api'

class DSService {

    constructor(private url: string = DS_API_URL, private data = {
        key: '75A7932396E4C33EA204C22E9EEBCEA8C53B15A3'
    }) {

    }

    public newPayment(order) {
        console.log('Pagamento iniciado...')

        const PaymentHeaders = new Headers({
            'Authorization':  `Basic ${btoa(this.data.key)}`,
            'Content-Type': 'application/x-www-form-urlencoded'
        })

        return fetch(this.url, {
            method: 'POST',
            headers: PaymentHeaders,
            body: JSON.stringify(order)
        })
    }
}


export default DSService
