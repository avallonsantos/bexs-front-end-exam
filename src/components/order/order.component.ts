/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): order.component.ts
 * @package DemoShop/Classes
 * --------------------------------------------------------------------------
 */

// Importando API Service

import './../app.service'
import DSService from "../app.service";

class Order {

    private apiService = new DSService()

    constructor() {
    }

    public makePayment(order) {
        // Aqui, para não dar erro, vou dar um return true, embora o que deve ser considerado, por favor, é o código subsequente
        alert('Show, a requisição de pagamento iniciaria aqui!')
        return true;
        // Aqui, para não dar erro, vou dar um return true, embora o que deve ser considerado, por favor, é o código subsequente
       this.apiService.newPayment(order).then(
            response => response.json(),
            error => console.log(error)
        )
    }
}

export default Order
