/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): url.api.ts
 * @package DemoShop/api
 * --------------------------------------------------------------------------
 */

const DS_API_URL = 'https://api.demoshop.com/v1/';

export default DS_API_URL
