/**
 * --------------------------------------------------------------------------
 * Checkout DemoShop (v1.0): checkout.component.ts
 * @package DemoShop/Classes
 * --------------------------------------------------------------------------
 */

// Importando scss correspondente ao componente checkout

import './checkout.component.scss'

// Importando plugin de máscara

import IMask from 'imask'

// Constants

import maskInputs from './utils/mask'

// Importando validador

import {cardIsValid as cardValidator} from './utils/card-validator'
import {dateIsValid as dateValidator} from './utils/date-validator'
import Pristine from './../../node_modules/pristinejs/src/pristine'
Pristine.addValidator('card-validator', cardValidator, 'Número de cartão inválido')
Pristine.addValidator('date-validator', dateValidator, 'Data inválida')

const validator = new Pristine(document.getElementById('payment-form'))

// Importando lib de identificação de cartão

import creditCardType from 'credit-card-type'

// Importando Classe de order

import Order from './order/order.component';
const order = new Order()

// Incialização da classe checkout

class Checkout {

    private fields: any
    private cardNumber = document.getElementById('card-number')
    private cardExpirationDate = document.getElementById('card-validate')
    private cardCVV = document.getElementById('card-cvv')

    constructor(private form: HTMLElement) {
        this.form = form
        this.fields = form.querySelectorAll('.input-labeled input')

        // Incialiando animações nos labels

        this.animateLabel()
        this.buildCardInformation()
        this.maskInputs()
        this.flipCard()
        this.formValidate()
        this.getCardType()
    }

    // Getters and setters

    get fieldsform() {
        return this.fields
    }

    get cNumber() {
        return this.cardNumber
    }

    get cExpiration() {
        return this.cardExpirationDate
    }

    get cCvv() {
        return this.cardCVV
    }

    // Função para animar o label nos inputs do checkout

    public animateLabel() {
        if(this.fieldsform !== undefined) {
            this.fieldsform.forEach(field => {
                field.addEventListener('focusin', () => {
                    field.parentNode.querySelector('label').classList.add('opened')
                })

                field.addEventListener('focusout', () => {
                    let fieldValue = field.value
                    if(fieldValue === '') {
                        field.parentNode.querySelector('label').classList.remove('opened')
                    }
                })
            })
        }
    }

    private writeModel(field: HTMLInputElement, target: HTMLElement) {
        let originalText: string = target.innerText
        field.addEventListener('keyup', () => {
            let modelValue = field.value
            if(modelValue.length > 0) {
                target.innerText = modelValue
                target.classList.add('filled')
            } else {
                target.innerText = originalText
                target.classList.remove('filled')
            }
        })
    }

    // Função para montar cartão à medida que é digitado

    public buildCardInformation() {
        if(this.fieldsform !== undefined) {
            this.fieldsform.forEach(field => {
                if(field.name === 'card-number') {
                    this.writeModel(field, document.querySelector('.card-number'))
                }

                if(field.name === 'card-name') {
                    this.writeModel(field, document.querySelector('.card-name'))
                }

                if(field.name === 'card-validate') {
                    this.writeModel(field, document.querySelector('.card-expiration-date'))
                }

                if(field.name === 'card-cvv') {
                    this.writeModel(field, document.querySelector('.card-cvv'))
                }
            })
        }
    }

    // Função para criar máscara nos inputs

    public maskInputs() {
        IMask(<HTMLInputElement>this.cNumber, maskInputs.maskCard)
        IMask(<HTMLInputElement>this.cExpiration, maskInputs.maskExpirationDate)
        IMask(<HTMLInputElement>this.cCvv, maskInputs.maskCVV)
    }


    // Função para animar cartão

    public flipCard() {
        this.cCvv.addEventListener('focusin', () => {
            document.querySelector('.credit-card-box').classList.add('flipped')
        })

        this.cardCVV.addEventListener('focusout', () => {
            document.querySelector('.credit-card-box').classList.remove('flipped')
        })

        document.querySelector('.credit-card-box').addEventListener('click', () => {
            document.querySelector('.credit-card-box').classList.toggle('flipped')
        })
    }

    // Validando formulário

    public formValidate() {
        this.form.addEventListener('submit', e => {
            e.preventDefault()
            let valid = validator.validate()
            if(valid) {
                const orderData = new FormData(this.form as HTMLFormElement)
                order.makePayment(orderData)
            }
        })
    }

    // Função para exibir logo da bandeira do cartão

    private appendBrandLogo(logo) {
        document.getElementById('brand-logo').removeAttribute('class')
        document.getElementById('brand-logo').classList.add(logo)
    }

    private resetBrandLogo() {
        document.getElementById('brand-logo').removeAttribute('class')
    }

    // Pegando tipo do cartão

    public getCardType() {
        this.cNumber.addEventListener('keyup', () => {
            let number = (this.cNumber as HTMLInputElement).value
            let cardBox = document.querySelector('.credit-card-box .card')
            let cardType = null
            if(number.length >= 2) {
                cardType = creditCardType(number).map(card => card.type).shift()
                if(cardType !== undefined) {
                    cardBox.classList.remove('default')
                    cardBox.classList.add('identified')
                } else {
                    cardBox.classList.remove('identified')
                    cardBox.classList.add('default')
                }
            } else {
                cardBox.classList.remove('identified')
                cardBox.classList.add('default')
            }

            if(cardType !== null) {
                switch (cardType) {
                    case 'visa':
                        this.appendBrandLogo('visa')
                        break;
                    case 'mastercard':
                        this.appendBrandLogo('mastercard')
                        break;
                    case 'diners-club':
                        this.appendBrandLogo('diners')
                        break;
                    case 'discover':
                        this.appendBrandLogo('discover')
                        break;
                    case 'american-express':
                        this.appendBrandLogo('american-express')
                        break;
                    case 'elo':
                        this.appendBrandLogo('elo')
                        break;
                }
            }

            if(cardType === undefined) {
                this.resetBrandLogo()
            }
        })
    }

}

export default Checkout
