// Importando Bootstrap 4 Grid

import './../node_modules/bootstrap-4-grid/scss/grid.scss';

// Importando funções úteis

import './components/utils/utils'

// Importing checkout

import Checkout from './components/checkout.component'

// Declarando checkout

window.addEventListener('load', () => {
    const checkout = new Checkout(document.getElementById('payment-form'))
})



