const path = require('path')
const merge = require('webpack-merge')
const commom = require('./webpack.common.js')
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// Inicializando configuração do Webpack em produção

module.exports = merge(commom, {
    mode: 'production',
    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
})
